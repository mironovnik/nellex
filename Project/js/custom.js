$(document).ready(function () {

    $('.burger').on('click', function () {
        $('.layer').toggleClass('active');
        $('.burger__menu').toggleClass('active').animate({left: -15}, 250);
    });

    $('.layer').on('click', function () {
        $(this).toggleClass('active');
        $('.burger__menu').toggleClass('active').animate({left: -316}, 250);
    });


    $('.checkbox-style__item').on('click', function () {
        $(this).find('.checkbox-style__checkbox').toggleClass('active');
    });

    $('.filter-category__filter-title').on('click', function () {
        $(this).find('.filter-title__icon').toggleClass('rotate');
        $(this).siblings('.filter-category__items').slideToggle();
    });

    $('.filter-brand__filter-title').on('click', function () {
        $(this).find('.filter-title__icon').toggleClass('rotate');
        $(this).siblings('.filter-brand__items').slideToggle();
    });

    $('.filter-color__filter-title').on('click', function () {
        $(this).find('.filter-title__icon').toggleClass('rotate');
        $(this).siblings('.filter-color__color-values').slideToggle();
    });

    $('.filter-category__item').on('click', function () {
        $(this).find('.filter-category__checkbox').toggleClass('active-inline');
    });

    $('.filter-brand__item').on('click', function () {
        $(this).find('.filter-brand__checkbox').toggleClass('active-inline');
    });

    $('.additional-photos__item').on('click', function (e) {
        $('.product-photos__additional-photos').children().removeClass('active-grey');
        var imageSrc = $(e.target).attr('src');
        $(this).addClass('active-grey');
        $('.product-photos__main-photo').find('img').attr('src', imageSrc);
        $('.product-photos__main-photo').find('img').attr('data-large', imageSrc);
    });

    $('.open-feedback').on('click', function () {
        $(this).siblings('.feedback-block-feedbacks').slideToggle();
    });
    $('.open-question').on('click', function () {
        $(this).siblings('.make-question').slideToggle();
    });
    $('.open-description').on('click', function () {
        $(this).siblings('.description__text').slideToggle();
    });
    $('.open-parameters').on('click', function () {
        $(this).siblings('.parameters-list').slideToggle();
    });
    $('.open-delivery').on('click', function () {
        $(this).siblings('.delivery__delivery-list').slideToggle();
    });
    $('.open-payment').on('click', function () {
        $(this).siblings('.payment__payment-list').slideToggle();
    });
    $('.product-checkbox').on('click', function () {
        $(this).find('.checkbox__enabled').toggleClass('active');
    });
    $('.sliderbx').bxSlider({
        controls: false,
        pagerSelector: '.slider-numbers__list',
        pagerType: 'full',
        auto: false,
        responsive: true,
        slideMargin: 5
    });
    $('.my-foto').on('mouseenter', function() {
        if($('.zoom-container').hasClass('active')) {
            $(this).removeClass('active');
            $(this).addClass('active');
        } else {
            $('.zoom-container').addClass('active');
        }
    });
    $('.my-foto').imagezoomsl({
        zoomrange: [1, 2],
        descarea: ".zoom-container",
    });
});