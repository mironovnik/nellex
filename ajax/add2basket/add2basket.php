<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$param = (array)json_decode($_REQUEST["param"]);
$product_id = $param['$productId'];
$productValue = $param['$productValue'];

if(CModule::IncludeModule("catalog") && $product_id > 0 && $productValue > 0)
{
    echo Add2BasketByProductID($product_id, $productValue, array(), array());
}
?>