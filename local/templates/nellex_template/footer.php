<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;
?>
<footer>
    <div class="footer-info">
        <div class="footer-info__main">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="subscribe">
                        <form action="" class="subscribe__form">
                            <div class="subscribe__title">Получать новости сайта</div>
                            <div class="subscribe__checkbox">
                                <input type="checkbox" id="check_news">
                                <label for="check_news">Получать только информацию по
                                    товарам</label>
                            </div>
                            <div class="subscribe__field">
                                <input type="text"><label>Подписаться</label>
                                <input type="submit">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="footer__menu">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="footer__menu-catalog">
                                    <ul class="menu-catalog__list">
                                        <li class="menu-catalog__list-item">
                                            <a href="" class="menu-catalog__list-link">Мужчины</a>
                                        </li>
                                        <li class="menu-catalog__list-item">
                                            <a href="" class="menu-catalog__list-link">Женщины</a>
                                        </li>
                                        <li class="menu-catalog__list-item">
                                            <a href=""
                                               class="menu-catalog__list-link">Аксессуары</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="footer__menu-about">
                                    <ul class="menu-about__list">
                                        <li class="menu-about__list-item">
                                            <a href="" class="menu-about__list-link">О брендах</a>
                                        </li>
                                        <li class="menu-about__list-item">
                                            <a href="" class="menu-about__list-link">Лукбуки</a>
                                        </li>
                                        <li class="menu-about__list-item">
                                            <a href="" class="menu-about__list-link">Контакты</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="footer__menu-social">
                                    <ul class="menu-social__list">
                                        <li class="menu-solial__list-item">
                                            <a class="menu-social__icon menu-social__icon--fb"></a>
                                            <a href="" class="list-item__name">facebook</a>
                                        </li>
                                        <li class="menu-solial__list-item">
                                            <a class="menu-social__icon menu-social__icon--inst"></a>
                                            <a href="" class="list-item__name">instagram</a>
                                        </li>
                                        <li class="menu-solial__list-item">
                                            <a class="menu-social__icon menu-social__icon--pint"></a>
                                            <a href="" class="list-item__name">pinterest</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-info__supporting">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="footer-info__logo">
                        <img src="img/white_logo.png" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="supporting__pay-systems">
                        <div class="row justify-content-between">
                            <div class="col-lg-2 col-md-2">
                                <div class="pay-system">
                                    <div class="pay-system__logo pay-system__logo--visa"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="pay-system">
                                    <div class="pay-system__logo pay-system__logo--mastercard"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="pay-system">
                                    <div class="pay-system__logo pay-system__logo--maestro"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12">
                    <div class="supporting__copyright">
                        <div class="copyright__company">Nellex@2018</div>
                        <div class="copyright__text">Все права защищены</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</body>
</html>
