<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nellex</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-2.2.4.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/sliderHits.js" defer></script>
    <script src="js/custom.js"></script>
</head>
<body>
<div class="layer"></div>
<div class="container-fluid">
    <header>
        <div class="row">
            <div class="col-lg-3">
                <div class="logo">
                    <a href="/"><img class="img-responsive" src="img/logo.png" alt="Логотип"></a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="main-menu">
                    <ul class="main-menu__list">
                        <li class="main-menu__list-item">
                            <a href="catalog.html" class="main-menu__list-link">Интернет-магазин</a>
                        </li>
                        <li class="main-menu__list-item">
                            <a href="" class="main-menu__list-link">Оптом</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="icon__block">
                    <div class="icons">
                        <div class="row justify-content-end">
                            <div class="col-lg-3">
                                <a href="">
                                    <div class="icons__icon icons__icon--profile"></div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-1">
                                <a href="">
                                    <div class="icons__icon icons__icon--cart"></div>
                                </a>
                            </div>
                            <div class="col-lg-2 col-md-1">
                                <a href="">
                                    <div class="product-counter">0</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile-header">
        <div class="burger">
            <div class="burger__line"></div>
            <div class="burger__line"></div>
            <div class="burger__line"></div>

        </div>
        <div class="logo">
            <a href="/"><img class="img-responsive" src="img/logo.png" alt="Логотип"></a>
        </div>
        <div class="icons">
            <div class="icons__icon icons__icon--cart"></div>
            <div class="product-counter">0</div>
        </div>
    </div>
