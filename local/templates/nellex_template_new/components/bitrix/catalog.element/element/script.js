(function () {
    window.addEventListener('load', function () {

        //переключатель дополнительных фотографий товара в карточке
        let additionalPhotosContainer = document.querySelector('.product-photos__additional-photos');
        let toggleAdditionalPhoto = function (e) {
            let additionalPhotos = document.querySelectorAll('.additional-photos__item');
            additionalPhotos.forEach(function (element) {
                element.classList.remove('active-grey');
            });
            var imageSrc = e.target.getAttribute('src');
            e.target.parentNode.classList.add('active-grey');
            let mainPhoto = document.querySelector('.product-photos__main-photo img');
            mainPhoto.setAttribute('src', imageSrc);
            mainPhoto.setAttribute('data-large', imageSrc);
        };
        additionalPhotosContainer.addEventListener('click', toggleAdditionalPhoto);

        //скрытие-раскрытие информационных блоков в карточке товара
        let slideToggleBlock = function (e) {
            if (e.target.classList.contains('show-block')) {
                let target = e.target;
                let nextElement = target.nextElementSibling;
                nextElement.classList.toggle('active');
            }
            if (e.target.parentNode.classList.contains('show-block')) {
                let target = e.target.parentNode;
                let nextElement = target.nextElementSibling;
                nextElement.classList.toggle('active');
            }
        };
        document.addEventListener('click', slideToggleBlock);

        //эмуляция добавления/удаления единиц товара
        let plusOneProduct = document.querySelector('.choose-quantity__btn--minus');
        let minusOneProduct = document.querySelector('.choose-quantity__btn--plus');
        let addOneProduct = function (e) {
            let target = e.target;
            let productValue = target.previousElementSibling.textContent;
            target.previousElementSibling.textContent = ++productValue;
            target.previousElementSibling.setAttribute('data-quantity', productValue);
        };
        let deleteOneProduct = function (e) {
            let target = e.target;
            let productValue = target.nextElementSibling.textContent;
            if (productValue > 1) {
                target.nextElementSibling.textContent = --productValue;
                target.nextElementSibling.setAttribute('data-quantity', productValue);
            }
        };
        plusOneProduct.addEventListener('click', addOneProduct);
        minusOneProduct.addEventListener('click', deleteOneProduct);

        //выбор цвета
        let chooseColor = function (e) {
            e.preventDefault();
            if (e.target.classList.contains('choose-color__colors')) {
                return 0;
            }
            document.querySelectorAll('.choose-color__color-value').forEach(function (element) {
                element.classList.remove('active-inline');
            });
            let choosenColor = e.target;
            choosenColor.classList.add('active-inline');
        };
        let productColorList = document.querySelector('.choose-color__colors');
        productColorList.addEventListener('click', chooseColor);
    });
})();