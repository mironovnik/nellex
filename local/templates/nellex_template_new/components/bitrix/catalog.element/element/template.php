<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<main>
    <div class="burger__menu">
        <ul class="burger-menu__burger-list">
            <li class="burger-menu__burger-item"><a href="/catalog/" class="burger-item__link">Интернет магазин</a></li>
            <li class="burger-menu__burger-item"><a href="/catalog/" class="burger-item__link">Оптом</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href='/' class="breadcrumbs__crumb">Главная</a>
                <span class="breadcrumbs__delimiter"></span>
                <a href="/catalog/" class="breadcrumbs__crumb">Каталог</a>
                <span class="breadcrumbs__delimiter"></span>
                <a href="/brands/<?= $arResult['PROPERTIES']['PRODUCT_BRAND']['VALUE'];?>/" class="breadcrumbs__crumb"><?= $arResult['PROPERTIES']['PRODUCT_BRAND']['VALUE'];?></a>
                <span class="breadcrumbs__delimiter"></span>
                <span class="breadcrumbs__crumb"><?=$arResult['NAME'];?></span>
            </div>
        </div>
    </div>
    <div class="product-cart">
        <div class="product-about">
            <div class="product-main-info">
                <div class="row">
                    <div class="col-lg-7 col-md-12">
                        <div class="product-about__product-photos">
                            <div class="row">
                                <div class="col-lg-10 col-md-12">
                                    <div class="product-photos">
                                        <div class="product-photos__main-photo">
                                            <div class="product-photos__zoom"></div>
                                                <img src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>"
                                                     data-large="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>"
                                                     data-imagezoom="true"
                                                     alt="<?= $arResult['NAME'] ?>">
                                            <? if(!empty($arResult['PROPERTIES']['NEW_PRODUCT']['VALUE'])) {
                                                ?><div class="product__marker product__marker--new">New</div><?
                                            }?>
                                            <? if(!empty($arResult['PROPERTIES']['SALE_PRODUCT']['VALUE'])) {
                                                ?><div class="product__marker product__marker--sale">Sale</div><?
                                            }?>
                                        </div>
                                        <div class="product-photos__under-photo-info">
                                            <div class="under-photo-info__brand-logo">
                                                <img src="<?= SITE_TEMPLATE_PATH . '/' ?>img/comp_logo1.png"
                                                     alt="<?= $arResult['PROPERTIES']['PRODUCT_BRAND']['VALUE']; ?>">
                                            </div>
                                            <div class="under-photo-info__social-block">
                                                <span class="social-icon social-icon--fb"></span>
                                                <span class="social-icon social-icon--inst"></span>
                                                <span class="social-icon social-icon--vk"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-12">
                                    <div class="product-photos__additional-photos">
                                        <div class="additional-photos__item active-grey">
                                            <img src="<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>"
                                                 alt="<?= $arResult['NAME'] ?>">
                                        </div>
                                        <?
                                        $obj = new CIBlockElement;
                                        $objImage = new CFile;
                                        foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $key => $arItem) {
                                            $Image = $objImage->GetPath($arItem); ?>
                                            <div class="additional-photos__item ">
                                                <img src="<?= $Image ?>" alt="<?= $arResult['NAME'] ?>">
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-12">
                        <div class="product-about__product-characters">
                            <div class="main-info__rating">
                                <? if(!empty($arItem['PROPERTIES']['PRODUCT_RATING']['VALUE'])) {
                                    $rating = $arItem['PROPERTIES']['PRODUCT_RATING']['VALUE'];
                                    $badRating = 5 - $rating;
                                    for ($i = 0; $i < $rating; $i++) {
                                        echo '<span class="rating__star rating__star--yes"></span>';
                                    }
                                    for ($i = 0; $i < $badRating; $i++) {
                                        echo '<span class="rating__star rating__star--no"></span>';
                                    }
                                } else {
                                    echo '<span class="rating__no-rating">Нет оценок</span>';
                                }
                                ?>
                            </div>
                            <div class="product-characters__product-name">
                                <span class="product-name__brand"><?= $arResult['PROPERTIES']['PRODUCT_BRAND']['VALUE']; ?></span>
                                <span class="product-name__delimiter">|</span>
                                <span class="product-name__name"><?= $arResult['NAME']; ?></span>
                            </div>
                            <div class="product-characters__product-price">
                                <span class="product-price__value"><?= $arResult['PRICES']['BASE']['VALUE'] ?>
                                    <span class="price__name"> руб.</span>
                                </span>
                            </div>
                            <div class="product-characters__product-article">
                                <span class="product-article__title">Артикул: </span>
                                <span class="product-article__value"><?= $arResult['PROPERTIES']['ARTICLE']['VALUE']; ?></span>
                            </div>
                            <div class="product-characters__product-composition">
                                <span class="product-composition__title">Состав:</span>
                                <span class="product-composition__composition-part">
                                    <? foreach ($arResult['PROPERTIES']['PRODUCT_COMPOSITION']['VALUE'] as $arComposition): ?>
                                        <span class="composition-part__name"><?= $arComposition; ?></span>
                                    <? endforeach; ?>
                                    </span>
                            </div>
                            <div class="product-characters__product-color">
                                <span class="product-color__title">Цвет: </span>
                                <span class="product-color__value"><?= $arResult['PROPERTIES']['PRODUCT_COLOR']['VALUE']; ?></span>
                            </div>
                            <div class="product-characters__choose-color">
                                <div class="choose-color__title">Выберите цвет:</div>
                                <div class="choose-color__colors">
                                    <? foreach ($arResult['PROPERTIES']['CHOOSE_COLOR']['VALUE'] as $key => $arColor): ?>
                                        <span class="choose-color__color-value <?if ($key < 1) echo 'active-inline';?>"
                                              style="background-color:#<?= $arColor; ?>"></span>
                                    <? endforeach; ?>
                                </div>
                            </div>
                            <div class="product-characters__choose-size">
                                <div class="choose-size__title">Выберите размер:</div>
                                <div class="choose-size__sizes">
                                    <? foreach ($arResult['PROPERTIES']['PRODUCT_SIZE']['VALUE'] as $arSize): ?>
                                        <span class="choose-size__size-value"><?= $arSize; ?></span>
                                    <? endforeach; ?>
                                </div>
                            </div>
                            <div class="product-characters__choose-quantity">
                                <div class="choose-quantity__title">Количество:</div>
                                <div class="choose-quantity__value">
                                    <span class="choose-quantity__btn choose-quantity__btn--plus">-</span>
                                    <span class="choose-quantity__value-text" data-quantity="1">1</span>
                                    <span class="choose-quantity__btn choose-quantity__btn--minus">+</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-6">
                    <div class="feedback-block">
                        <div class="feedback-block__title-block show-block">
                            <span class="title-block__title ">Отзывы</span>
                            <span class="title-block__arrow-open"></span>
                        </div>
                        <div class="feedback-block-feedbacks active">
                            <div class="feedback-block__feedback">
                                <div class="feedback__feedback-info">
                                    <span class="feedback-info__user-avatar"></span>
                                    <span class="feedback-info__user-name">Илья</span>
                                    <span class="feedback-info__feedback-date">05.02.19</span>
                                    <span class="feedback-info__feedback-time">13:20</span>
                                </div>
                                <div class="feedback__feedback-product-info">
                                        <span class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </span>
                                    <span class="feedback__choosen-color">
                                            <span class="choosen-color__title">Цвет: </span>
                                            <span class="choosen-color__value">Пудровый</span>
                                        </span>
                                    <span class="feedback__choosen-size">
                                            <span class="choosen-size__title">Размер: </span>
                                            <span class="choosen-size__value">56</span>
                                        </span>
                                </div>
                                <div class="feedback__feedback-text">
                                    Шикарная шапка! Мягкая и ооочень тёплая. Может кому-то слегка колко от
                                    шерсти. Но я этим не страдаю. Мне очень комфортно в ней.
                                </div>
                            </div>
                        </div>
                        <div class="feedback-block__title-block show-block">
                            <span class="title-block__title">Задать вопрос</span>
                            <span class="title-block__arrow-open "></span>
                        </div>
                        <div class="feedback-block__feedback make-question active">
                            <div class="feedback__feedback-info">
                                <span class="feedback-info__user-avatar"></span>
                                <span class="feedback-info__user-name">Илья</span>
                            </div>
                            <div class="feedback__feedback-text">
                                <form action="">
                                    <textarea class="feedback-text__field" name="" id="" cols="30" rows="10"></textarea>
                                    <input class="feedback-text__button" type="submit" value="Отправить">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="product-parameters">
                        <div class="product-parameters__product-add-to">
                            <div class="product-add-to__cart" data-id="<?=$arResult['ID']?>">
                                <span class="cart__title">Добавить в корзину</span>
                                <span class="cart__icon"></span>
                            </div>
                            <div class="product-add-to__favorite">
                                <span class="favorite__icon"></span>
                            </div>
                        </div>
                        <div class="product-parameters__description">
                            <div class="description__title show-block">
                                <span class="description__title-text">Описание</span>
                                <span class="description__title-icon"></span>
                            </div>
                            <div class="description__text active">
                                <?= $arResult['DETAIL_TEXT']; ?>
                            </div>
                        </div>
                        <div class="product-parameters__parameters-list">
                            <div class="description__title show-block">
                                <span class="description__title-text">Параметры</span>
                                <span class="description__title-icon"></span>
                            </div>
                            <div class="parameters-list active">
                                <div class="parameters-list__parameters-item">
                                    <span class="parameters-item__name">Вид застежки: </span>
                                    <span class="parameters-item__value">без застежки</span>
                                </div>
                                <div class="parameters-list__parameters-item">
                                    <span class="parameters-item__name">Фактура материала: </span>
                                    <span class="parameters-item__value">вязаный</span>
                                </div>
                                <div class="parameters-list__parameters-item">
                                    <span class="parameters-item__name">Декортаивные элементы: </span>
                                    <span class="parameters-item__value">без элементов</span>
                                </div>
                                <div class="parameters-list__parameters-item">
                                    <span class="parameters-item__name">Материал подкладки: </span>
                                    <span class="parameters-item__value">без подкладки</span>
                                </div>
                                <div class="parameters-list__parameters-item">
                                    <span class="parameters-item__name">Модель шапки: </span>
                                    <span class="parameters-item__value">бини</span>
                                </div>
                            </div>
                        </div>
                        <div class="product-parameters__delivery ">
                            <div class="description__title show-block">
                                <span class="description__title-text">Доставка</span>
                                <span class="description__title-icon"></span>
                            </div>
                            <div class="delivery__delivery-list active">
                                <span class="delivery-list__delivery-item">
                                    <input type="radio" name="delivery" id="delivery-item-first" checked >
                                    <label for="delivery-item-first">Самовывоз</label>
                                </span>
                                <span class="delivery-list__delivery-item">
                                    <input type="radio" name="delivery" id="delivery-item-second" >
                                    <label for="delivery-item-second">курьером по Москве и МО</label>
                                </span>
                                <span class="delivery-list__delivery-item">
                                    <input type="radio" name="delivery" id="delivery-item-third" >
                                    <label for="delivery-item-third">почтой РФ</label>
                                </span>
                            </div>
                        </div>
                        <div class="product-parameters__payment">
                            <div class="description__title show-block">
                                <span class="description__title-text">Оплата</span>
                                <span class="description__title-icon"></span>
                            </div>
                            <div class="payment__payment-list active">
                                <span class="payment-list__payment-item">
                                    <input type="radio" name="payment" id="payment-cash" checked>
                                    <label for="payment-cash">наличный расчет</label>
                                </span>
                                <span class="payment-list__payment-item">
                                    <input type="radio" name="payment" id="payment-cod">
                                    <label for="payment-cod">Наложенный платеж</label>
                                </span>
                                <div class="payment-list__payment-systems">
                                    <span class="payment-systems__system-item">
                                        <input type="radio" name="payment" id="payment-yandex">
                                        <label for='payment-yandex' class="system-item__system-icon system-item__system-icon--yandex"></label>
                                    </span>
                                    <span class="payment-systems__system-item">
                                        <input type="radio" name="payment" id="payment-visa">
                                        <label for='payment-visa' class="system-item__system-icon system-item__system-icon--visa"></label>
                                    </span>
                                    <span class="payment-systems__system-item">
                                        <input type="radio" name="payment" id="payment-mastercard">
                                        <label for='payment-mastercard' class="system-item__system-icon system-item__system-icon--mastercard"></label>
                                    </span>
                                    <span class="payment-systems__system-item">
                                        <input type="radio" name="payment" id="payment-maestro">
                                        <label for='payment-maestro' class="system-item__system-icon system-item__system-icon--maestro"></label>
                                    </span>
                                </div>
                                <span class="payment-list__payment-item">
                                    <span class="payment-list__payment-item">
                                    <input type="radio" name="payment" id="payment-card">
                                    <label for="payment-card">безналичный расчет</label>
                                </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <div class="product-slider" style="display: none">
                    <div class="slider-arrow-block">
                        <div class="slider-arrow slider-arrow--left"></div>
                    </div>
                    <div class="viewed-products">
                        <div class="viewed-products__title">Вы смотрели</div>
                        <div class="viewed-products__list">
                            <div class="viewed-products__list slider-viewed">
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span
                                                        class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span
                                                        class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span
                                                        class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span
                                                        class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span
                                                        class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span
                                                        class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span
                                                        class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span
                                                        class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span
                                                        class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span
                                                        class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span
                                                        class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span
                                                        class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product__image">
                                        <img src="img/catalog-product.jpg" alt="">
                                        <div class="product__marker product__marker--sale">Sale</div>
                                    </div>
                                    <div class="product__main-info">
                                        <div class="main-info__name-icons">
                                            <div class="name-icons__flex-item">
                                                <span class="name-icons__brand-name">Vilermo</span>
                                                <span class="name-icons__delimiter">|</span>
                                                <span class="name-icons__product-name">Шапка Luciano</span>
                                            </div>
                                            <div class="name-icons__flex-icons">
                                                <span class="name-icons__icon name-icons__icon--favor"></span>
                                                <span class="name-icons__icon name-icons__icon--cart"></span>
                                            </div>
                                        </div>
                                        <div class="main-info__price">
                                            <span class="price__value">2000<span class="price__name"> руб.</span></span>
                                            <span class="price__old-value">1000<span class="price__name"> руб.</span></span>
                                            <span class="price__sale-value">-33<span class="price__sale-name">%</span></span>
                                        </div>
                                        <div class="main-info__size-available">
                                            <span class="size-available__title">Размер в наличии: </span>
                                            <span class="size-available__value"> 56 </span>
                                        </div>
                                        <div class="main-info__rating">
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--yes"></span>
                                            <span class="rating__star rating__star--no"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-arrow-block">
                        <div class="slider-arrow slider-arrow--right"></div>
                    </div>
                </div>
    </div>
</main>