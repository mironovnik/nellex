<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(false);
?>
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
$arDir = explode('/', $dir);
$arDir = array_diff($arDir, array(''));
?>
<main>
    <div class="burger__menu">
        <ul class="burger-menu__burger-list">
            <li class="burger-menu__burger-item"><a href="" class="burger-item__link">Интернет магазин</a></li>
            <li class="burger-menu__burger-item"><a href="" class="burger-item__link">Оптом</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs">
                <a href="/" class="breadcrumbs__crumb">Главная</a>
                <span class="breadcrumbs__delimiter"></span>
                <? if($arDir[1] == 'brands') { ?>
                    <a href="/brands/" class="breadcrumbs__crumb">Бренды</a>
                    <span class="breadcrumbs__delimiter"></span>
                    <span class="breadcrumbs__crumb"><?=$arDir[2];?></span>
                <? } else {
                    ?><span class="breadcrumbs__crumb">Каталог</span> <?
                } ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="main-title">Каталог</div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="filter">
                <div class="filter__checkbox-style">
                    <div class="checkbox-style__item">
                        <span class="checkbox-style__name">Детское</span>
                        <span class="checkbox-style__checkbox">
                                <span class="checkbox__enabled"></span>
                            </span>
                    </div>
                </div>
                <div class="filter-category">
                    <div class="filter-category__filter-title">
                        <span class="filter-title__value">Пол</span>
                        <span class="filter-title__icon"></span>
                    </div>
                    <div class="filter-category__items">
                        <div class="filter-category__item">
                                <span class="filter-category__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-category__name">Женский</span>
                        </div>
                        <div class="filter-category__item">
                                <span class="filter-category__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-category__name">Мужской</span>
                        </div>
                    </div>
                </div>
                <div class="filter-category">
                    <div class="filter-category__filter-title">
                        <span class="filter-title__value">Категория</span>
                        <span class="filter-title__icon"></span>
                    </div>
                    <div class="filter-category__items">
                        <div class="filter-category__item">
                                <span class="filter-category__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-category__name">Шапки</span>
                        </div>
                        <div class="filter-category__item">
                                <span class="filter-category__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-category__name">Шарфы</span>
                        </div>
                        <div class="filter-category__item">
                                <span class="filter-category__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-category__name">Сноуты</span>
                        </div>
                    </div>
                </div>
                <div class="filter-brand">
                    <div class="filter-brand__filter-title">
                        <span class="filter-title__value">Бренд</span>
                        <span class="filter-title__icon"></span>
                    </div>
                    <div class="filter-brand__items">
                        <div class="filter-brand__item">
                                <span class="filter-brand__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-brand__name">Vilermo</span>
                        </div>
                        <div class="filter-brand__item">
                                <span class="filter-brand__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-brand__name">Negarzo</span>
                        </div>
                        <div class="filter-brand__item">
                                <span class="filter-brand__checkbox">
                                    <span class="checkbox__enabled"></span>
                                </span>
                            <span class="filter-brand__name">Neil</span>
                        </div>
                    </div>
                </div>
                <div class="filter-color">
                    <div class="filter-color__filter-title">
                        <span class="filter-title__value">Цвет</span>
                        <span class="filter-title__icon"></span>
                    </div>
                    <div class="filter-color__color-values">
                        <div class="filter-color__value filter-color__value--black"></div>
                        <div class="filter-color__value filter-color__value--grey"></div>
                        <div class="filter-color__value filter-color__value--brown"></div>
                        <div class="filter-color__value filter-color__value--pink"></div>
                        <div class="filter-color__value filter-color__value--dark-yellow"></div>
                        <div class="filter-color__value filter-color__value--red"></div>
                        <div class="filter-color__value filter-color__value--blue"></div>
                        <div class="filter-color__value filter-color__value--green"></div>
                    </div>
                </div>
                <div class="filter-price">
                    <div class="filter-price__filter-title">
                        <span class="filter-title__value">Цена</span>
                        <span class="filter-title__icon"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-12 col-sm-12">
            <div class="catalog-list">
                <div class="row">
                    <?foreach($arResult["ITEMS"] as $arItem ):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-lg-3 col-md-6 col-sm-12">
                        <div class="product">
                            <div class="product__image">
                                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME'];?>">
                                <? if(!empty($arItem['PROPERTIES']['NEW_PRODUCT']['VALUE'])) {
                                    ?><div class="product__marker product__marker--new">New</div><?
                                }?>
                                <? if(!empty($arItem['PROPERTIES']['SALE_PRODUCT']['VALUE'])) {
                                    ?><div class="product__marker product__marker--sale">Sale</div><?
                                }?>

                            </div>
                            <div class="product__main-info">
                                <div class="main-info__name-icons">
                                    <div class="name-icons__flex-item">
                                        <a href="/brands/<?=$arItem['PROPERTIES']['PRODUCT_BRAND']['VALUE']?>/" class="name-icons__brand-name"><?=$arItem['PROPERTIES']['PRODUCT_BRAND']['VALUE']?></a>
                                        <span class="name-icons__delimiter">|</span>
                                        <a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="name-icons__product-name"><?=$arItem['NAME'];?></a>
                                    </div>
                                    <div class="name-icons__flex-icons">
                                        <span class="name-icons__icon name-icons__icon--favor" title="Добавить в избранное"></span>
                                        <div class="product-add-to__cart" data-id="<?=$arItem['ID']?>">
                                            <span class="name-icons__icon name-icons__icon--cart " title="Добавить в корзину" ></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="main-info__price">
                                    <span class="price__value"><?=$arItem['PRICES']['BASE']['VALUE'];?><span class="price__name"> руб.</span></span>
                                </div>
                                <div class="main-info__size-available">
                                    <span class="size-available__title">Размер в наличии: </span>
                                    <?foreach($arItem['PROPERTIES']['PRODUCT_SIZE']['VALUE'] as $arSize):?>
                                    <span class="size-available__value"><?=$arSize;?></span>
                                    <?endforeach;?>
                                </div>
                                <div class="main-info__rating">
                                    <? if(!empty($arItem['PROPERTIES']['PRODUCT_RATING']['VALUE'])) {
                                        $rating = $arItem['PROPERTIES']['PRODUCT_RATING']['VALUE'];
                                        $badRating = 5 - $rating;
                                        for ($i = 0; $i < $rating; $i++) {
                                            echo '<span class="rating__star rating__star--yes"></span>';
                                        }
                                        for ($i = 0; $i < $badRating; $i++) {
                                            echo '<span class="rating__star rating__star--no"></span>';
                                        }
                                    } else {
                                        echo '<span class="rating__no-rating">Нет оценок</span>';
                                    }
                                     ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
</main>