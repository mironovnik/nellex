<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(false);
?>
<div class="hits-slider">
    <div class="control-elements">
        <div class="row">
            <div class="col-lg-1 col-md-1">
                <div class="hits-slider__arrow-icon hits-slider__arrow-icon--left">
                </div>
            </div>
            <div class="col-lg-10 col-md-10">
                <div class="hits-slider__title">
                    <div class="hits-slider__title-value">
                        Хиты продаж
                    </div>
                    <a href="/catalog/" class="hits-slider__link">В магазин</a>
                </div>
            </div>
            <div class="col-lg-1 col-md-1">
                <div class="hits-slider__arrow-icon hits-slider__arrow-icon--right">
                </div>
            </div>
        </div>
    </div>
    <div class="products-hit sliderbx-hit clearfix">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="product-hit">
                <div class="product-hit__image">
                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<?= $arItem['NAME']; ?>">
                </div>
                <div class="product-hit__description">
                    <div class="product-hit__brand">
                        <a href="/brands/<?=$arItem['PROPERTIES']['PRODUCT_BRAND']['VALUE']?>/"><?= $arItem['PROPERTIES']['PRODUCT_BRAND']['VALUE']; ?></a>
                    </div>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        <div class="product-hit__name">
                            <span class="product-hit__articul-value"><?= $arItem['PROPERTIES']['ARTICLE']['VALUE']; ?></span>
                            <span class="product-hit__name-value"><?= $arItem['NAME']; ?></span>
                        </div>
                    </a>
                    <div class="product-hit__cost">
                        <?= $arItem['PRICES']['BASE']['VALUE'] ?> р
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>
