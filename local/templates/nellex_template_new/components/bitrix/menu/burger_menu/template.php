<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="burger__menu">
        <ul class="burger-menu__burger-list">

            <?
            foreach ($arResult as $arItem):
                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue;
                ?>
                <? if ($arItem["SELECTED"]):?>
                <li class="burger-menu__burger-item">
                    <a href="<?= $arItem["LINK"] ?>" class=" burger-item__link active_link"><?= $arItem["TEXT"] ?></a>
                </li>
            <? else:?>
                <li class="burger-menu__burger-item">
                    <a href="<?= $arItem["LINK"] ?>" class="burger-item__link"><?= $arItem["TEXT"] ?></a>
                </li>
            <? endif ?>

            <? endforeach ?>

        </ul>
    </div>
<? endif ?>