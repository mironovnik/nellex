<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <div class="col-lg-4 col-md-12">
        <div class="footer__menu-about">
            <ul class="menu-about__list">
                <?foreach($arResult as $arItem):
                    if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                        continue;
                    ?>
                    <?if($arItem["SELECTED"]):?>
                    <li class="menu-about__list-item"><a href="<?=$arItem["LINK"]?>" class=" menu-about__list-link active_link"><?=$arItem["TEXT"]?></a></li>
                <?else:?>
                    <li class="menu-about__list-item"><a href="<?=$arItem["LINK"]?>"  class="menu-about__list-link"><?=$arItem["TEXT"]?></a></li>
                <?endif?>
                <?endforeach?>
            </ul>
        </div>
    </div>

<?endif?>