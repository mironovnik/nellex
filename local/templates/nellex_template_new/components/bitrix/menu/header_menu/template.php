<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?if (!empty($arResult)):?>
<div class="col-lg-6">
    <nav class="main-menu">
        <ul class="main-menu__list">

        <?
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <?if($arItem["SELECTED"]):?>
            <li class="main-menu__list-item"><a href="<?=$arItem["LINK"]?>" class=" main-menu__list-link active_link"><?=$arItem["TEXT"]?></a></li>
        <?else:?>
            <li class="main-menu__list-item"><a href="<?=$arItem["LINK"]?>"  class="main-menu__list-link"><?=$arItem["TEXT"]?></a></li>
        <?endif?>

        <?endforeach?>

        </ul>
    </nav>
</div>
<?endif?>
