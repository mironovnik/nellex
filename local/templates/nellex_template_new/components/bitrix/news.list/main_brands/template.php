<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="about-brand">
    <div class="row">
        <?foreach($arResult['ITEMS'] as $arItems):?>
        <div class="col-lg-4 col-md-12">
            <div class="brand-info">
                <div class="brand-info__logo">
                    <img src="<?=$arItems['PREVIEW_PICTURE']['SRC']?>" alt="">
                </div>
                <div class="brand-info__description">
                    <?=$arItems['PREVIEW_TEXT']?>
                </div>
                <div class="brand-info__more-info">
                    <a href="/brands/vilermo/" class="btn">Подробнее</a>
                </div>
            </div>
        </div>
        <?endforeach;?>
    </div>
</div>