<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="slider-layer">
    <div class="main-slider">
        <div class="row">
            <div class="col-lg-2 col-md-12">
                <div class="slider-numbers">
                    <ul class="slider-numbers__list">
                        <? foreach($arResult['ITEMS'] as $arItems):?>
                        <li class="slider-numbers__list-item"></li>
                        <?endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="sliderbx">
                    <? foreach($arResult['ITEMS'] as $arItems):?>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="slider-info">
                                <div class="slider-info__title"><?=$arItems['PREVIEW_TEXT'];?></div>
                                <div class="slider-info__logo">
                                    <?
                                    $file = new CFile;
                                    $file_path = $file->GetPath($arResult['PROPERTIES']['BRAND_PHOTO']['VALUE']);
                                    ?>
                                    <img src="<?=SITE_TEMPLATE_PATH.'/'?>img/company_logo.png" alt="">
                                </div>
                                <div class="slider-info__description"><?=$arItems['DETAIL_TEXT'];?></div>
                                <div class="slider-info__button">
                                    <a href="/catalog/" class="btn btn--buy">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="slide">
                                <img src="<?=$arItems['PREVIEW_PICTURE']['SRC'];?>" alt="Слайд1">
                            </div>
                        </div>
                    </div>
                    <?endforeach;?>
                </div>
            </div>

        </div>
    </div>
</div>

