<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="new-products">
    <div class="row">
        <? foreach($arResult['ITEMS'] as $key => $arItems):?>
        <div class="col-lg-4 col-md-12 ">
            <div class="new-products__item <?if ($key == 1) echo 'd-flex flex-column'?>" >
                <div class="new-products__info <?if ($key == 1) echo 'order-2'?>">
                    <div class="new-products__title">
                        <?=$arItems['NAME'];?>
                    </div>
                    <div class="new-products__description">
                        <?=$arItems['PREVIEW_TEXT']?>
                    </div>
                    <div class="new-products__to-catalog">
                        <a href="/catalog/" class="to-catalog__link">В магазин</a>
                    </div>
                </div>
                <div class="new-products__image <?if ($key == 1) echo 'order-1'?>">
                    <img src="<?=$arItems['PREVIEW_PICTURE']['SRC']?>" alt="">
                </div>
            </div>
        </div>
        <?endforeach;?>
    </div>
</div>