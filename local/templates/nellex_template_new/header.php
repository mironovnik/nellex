<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap-grid.min.css");?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-2.2.4.js");?>
    <?$APPLICATION->ShowHead()?>
</head>
<?$APPLICATION->ShowPanel()?>
<body>
<div class="layer"></div>
<div class="container-fluid">
    <header>
        <div class="row">
            <div class="col-lg-3">
                <div class="logo">
                    <a href="/"><img class="img-responsive" src="<?=SITE_TEMPLATE_PATH.'/'?>img/logo.png" alt="Логотип"></a>
                </div>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "header_menu", Array(
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                    0 => "",
                ),
                "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "ROOT_MENU_TYPE" => "header",	// Тип меню для первого уровня
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>

            <div class="col-lg-3">
                <div class="icon__block">
                    <div class="icons">
                        <div class="row justify-content-end">
                            <div class="col-lg-3">
                                <a href="/personal/">
                                    <div class="icons__icon icons__icon--profile"></div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-1">
                                <a href="/personal/cart/">
                                    <div class="icons__icon icons__icon--cart"></div>
                                </a>
                            </div>
                            <div class="col-lg-2 col-md-1">
                                <a href="/personal/cart/">
                                    <div class="product-counter"><?=getCountBasket()?></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile-header">
        <div class="burger">
            <div class="burger__line"></div>
            <div class="burger__line"></div>
            <div class="burger__line"></div>
        </div>
        <div class="logo">
            <a href="/"><img class="img-responsive" src="<?=SITE_TEMPLATE_PATH.'/'?>img/logo.png" alt="Логотип"></a>
        </div>
        <div class="icons">
            <div class="icons__icon icons__icon--cart"></div>
            <div class="product-counter"><?=getCountBasket()?></div>
        </div>
    </div>
