(function () {
    //открытие-закрытие мобильного меню
    let burger = document.querySelector('.burger');
    let layer = document.querySelector('.layer');
    let openBurgerMenu = function () {
        let burgerMenu = document.querySelector('.burger__menu');
        layer.classList.toggle('active');
        burgerMenu.classList.toggle('active-burger');
    };
    let closeBurgerMenu = function (e) {
        let burgerMenu = document.querySelector('.burger__menu');
        e.target.classList.toggle('active');
        burgerMenu.classList.toggle('active-burger');
    };
    burger.addEventListener('click', openBurgerMenu);
    layer.addEventListener('click', closeBurgerMenu);



    //ajax-запросы добавления в корзину и обновления счетчика товара
    let addToCart = function (e) {
        let target = e.target;
        if (target.parentNode.classList.contains('product-add-to__cart')) {
            let productId = target.parentNode.getAttribute('data-id');
            let productValueField = document.querySelector('.choose-quantity__value-text');
            let productValue;
            if (productValueField) {
                productValue = productValueField.getAttribute('data-quantity');
            } else {
                productValue = 1;
            }

            let data = {$productId: productId, $productValue: productValue};

            let url = '/ajax/add2basket/add2basket.php';
            let xhr = new XMLHttpRequest(); //создаем объект XMLHttpRequest
            xhr.addEventListener('load', function () { //обработчик ответа сервера
                if (xhr.status === 200) { //если статус ответа успешно
                    BX.onCustomEvent('OnBasketChange');
                    alert('Продукт добавлен в корзину');
                }
            });
            xhr.open('POST', url); //открываем соединение
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send('param=' + JSON.stringify(data)); //отправляем данные

            //Ajax-запрос на обновление счетчика корзины
            let xhrUpdateCart = new XMLHttpRequest();
            let urlUpdateCart = '/ajax/update_count/update_count.php';
            xhrUpdateCart.addEventListener('load', function () {
                if (xhrUpdateCart.status === 200) {
                    let productCounter = document.querySelector('.product-counter');
                    productCounter.textContent = xhrUpdateCart.responseText;
                }
            });
            xhrUpdateCart.open('POST', urlUpdateCart);
            xhrUpdateCart.send();
        }

    };
    document.addEventListener('click', addToCart);

})();