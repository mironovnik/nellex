(function(){
    var bxParams = {};
    if (window.innerWidth < 992) {
        bxParams = {
            controls: false,
            pager: true,
            pagerType: 'short',
            pagerShortSeparator: ' из ',
            minSlides: 2,
            maxSlides: 2,
            moveSlides: 2,
            slideMargin: 10,
            slideWidth: 450,
            auto: false,
            responsive: true
        };
    }
    if (window.innerWidth > 992 && window.innerWidth < 1200) {
        bxParams = {
            controls: true,
            pager: false,
            minSlides: 5,
            maxSlides: 5,
            moveSlides: 1,
            slideWidth: 224,
            slideMargin: 20,
            nextSelector: '.hits-slider__arrow-icon--right',
            prevSelector: '.hits-slider__arrow-icon--left',
            auto: true,
            responsive: true
        }
    }
    if (window.innerWidth > 1200) {
        bxParams = {
            controls: true,
            pager: false,
            minSlides: 5,
            maxSlides: 5,
            moveSlides: 1,
            slideWidth: 510,
            slideMargin: 20,
            nextSelector: '.hits-slider__arrow-icon--right',
            prevSelector: '.hits-slider__arrow-icon--left',
            auto: true,
            responsive: true
        }
    }
    var sliderHits = $('.sliderbx-hit').bxSlider(bxParams);
    $(window).on('resize', function() {
        if (window.innerWidth < 992) {
            bxParams = {
                controls: false,
                pager: true,
                pagerType: 'short',
                pagerShortSeparator: ' из ',
                minSlides: 2,
                maxSlides: 2,
                moveSlides: 2,
                slideMargin: 10,
                slideWidth: 450,
                auto: false,
                responsive: true
            };
            sliderViewedParams = {
                controls: false,
                minSlides: 2,
                maxSlides: 2,
                moveSlides: 1,
                slideWidth: 300,
                slideMargin: 20,
                auto: true,
                responsive: true

            };
            sliderHits.reloadSlider(bxParams);
        }
        if (window.innerWidth > 992 && window.innerWidth < 1200) {
            bxParams = {
                controls: true,
                pager: false,
                minSlides: 5,
                maxSlides: 5,
                moveSlides: 1,
                slideWidth: 224,
                slideMargin: 20,
                nextSelector: '.hits-slider__arrow-icon--right',
                prevSelector: '.hits-slider__arrow-icon--left',
                auto: true,
                responsive: true
            };
            sliderHits.reloadSlider(bxParams);
        }
        if (window.innerWidth > 1200) {
            bxParams = {
                controls: true,
                pager: false,
                minSlides: 5,
                maxSlides: 5,
                moveSlides: 1,
                slideWidth: 510,
                slideMargin: 20,
                nextSelector: '.hits-slider__arrow-icon--right',
                prevSelector: '.hits-slider__arrow-icon--left',
                auto: true,
                responsive: true
            };
            sliderHits.reloadSlider(bxParams);
        }
    });
})();

