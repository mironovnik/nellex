(function() {

    //инициализация первого слайдера на главной
    $('.sliderbx').bxSlider({
        controls: false,
        pagerSelector: '.slider-numbers__list',
        pagerType: 'full',
        auto: false,
        responsive: true,
        slideMargin: 5
    });

})();