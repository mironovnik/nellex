(function(){
    var sliderViewedParams ={};
    if (window.innerWidth <= 480) {
        sliderViewedParams = {
            controls: false,
            pager: false,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideWidth: 250,
            slideMargin: 50,
            auto: true,
            responsive: true
        };
        console.log(window.screen.width);
        console.log(sliderViewedParams);

    }
    if (window.innerWidth > 480 && window.innerWidth <= 764) {
        sliderViewedParams = {
            controls: false,
            pager: false,
            minSlides: 2,
            maxSlides: 2,
            moveSlides: 1,
            slideWidth: 180,
            slideMargin: 50,
            auto: true,
            responsive: true

        };
    }
    if (window.innerWidth > 764 && window.innerWidth <= 992) {
        sliderViewedParams = {
            controls: false,
            pager: false,
            minSlides: 2,
            maxSlides: 2,
            moveSlides: 1,
            slideWidth: 300,
            slideMargin: 20,
            auto: true,
            responsive: true

        };

    }
    if (window.innerWidth > 992 && window.innerWidth <= 1200) {
        sliderViewedParams = {
            controls: true,
            pager: false,
            minSlides: 4,
            maxSlides: 4,
            moveSlides: 1,
            slideWidth: 210,
            slideMargin: 20,
            nextSelector: '.slider-arrow--right',
            prevSelector: '.slider-arrow--left',
            auto: true,
            responsive: true

        };

    }
    if (window.innerWidth > 1200 && window.innerWidth <= 1600) {
        sliderViewedParams = {
            controls: true,
            pager: false,
            minSlides: 5,
            maxSlides: 5,
            moveSlides: 1,
            slideWidth: 190,
            slideMargin: 20,
            nextSelector: '.slider-arrow--right',
            prevSelector: '.slider-arrow--left',
            auto: true,
            responsive: true

        };

    }
    if (window.innerWidth > 1600 && window.innerWidth <= 1920) {
        sliderViewedParams = {
            controls: true,
            pager: false,
            minSlides: 6,
            maxSlides: 6,
            moveSlides: 1,
            slideWidth: 200,
            slideMargin: 20,
            nextSelector: '.slider-arrow--right',
            prevSelector: '.slider-arrow--left',
            auto: true,
            responsive: true

        };

    }
    if (window.innerWidth > 1920 && window.innerWidth <= 2560) {
        sliderViewedParams = {
            controls: true,
            pager: false,
            minSlides: 7,
            maxSlides: 7,
            moveSlides: 1,
            slideWidth: 200,
            slideMargin: 20,
            nextSelector: '.slider-arrow--right',
            prevSelector: '.slider-arrow--left',
            auto: true,
            responsive: true

        };

    }
    if (window.innerWidth > 2560) {
        sliderViewedParams = {
            controls: true,
            pager: false,
            minSlides: 9,
            maxSlides: 9,
            moveSlides: 1,
            slideWidth: 220,
            slideMargin: 20,
            nextSelector: '.slider-arrow--right',
            prevSelector: '.slider-arrow--left',
            auto: true,
            responsive: true

        };
    }

    var sliderViewed = $('.slider-viewed').bxSlider(sliderViewedParams);

    $(window).on('resize', function() {
        if (window.innerWidth <= 480) {
            sliderViewedParams = {
                controls: false,
                pager: false,
                minSlides: 1,
                maxSlides: 1,
                moveSlides: 1,
                slideWidth: 250,
                slideMargin: 50,
                auto: true,
                responsive: true

            };
        }
        if (window.innerWidth > 480 && window.innerWidth <= 764) {
            sliderViewedParams = {
                controls: false,
                pager: false,
                minSlides: 2,
                maxSlides: 2,
                moveSlides: 1,
                slideWidth: 180,
                slideMargin: 50,
                auto: true,
                responsive: true

            };
        }
        if (window.innerWidth > 764 && window.innerWidth <= 992) {
            sliderViewedParams = {
                controls: false,
                pager: false,
                minSlides: 2,
                maxSlides: 2,
                moveSlides: 1,
                slideWidth: 300,
                slideMargin: 20,
                auto: true,
                responsive: true

            };

        }
        if (window.innerWidth > 992 && window.innerWidth <= 1200) {
            sliderViewedParams = {
                controls: true,
                pager: false,
                minSlides: 4,
                maxSlides: 4,
                moveSlides: 1,
                slideWidth: 210,
                slideMargin: 20,
                nextSelector: '.slider-arrow--right',
                prevSelector: '.slider-arrow--left',
                auto: true,
                responsive: true

            };

        }
        if (window.innerWidth > 1200 && window.innerWidth <= 1600) {
            sliderViewedParams = {
                controls: true,
                pager: false,
                minSlides: 5,
                maxSlides: 5,
                moveSlides: 1,
                slideWidth: 200,
                slideMargin: 20,
                nextSelector: '.slider-arrow--right',
                prevSelector: '.slider-arrow--left',
                auto: true,
                responsive: true

            };

        }
        if (window.innerWidth > 1600 && window.innerWidth <= 1920) {
            sliderViewedParams = {
                controls: true,
                pager: false,
                minSlides: 6,
                maxSlides: 6,
                moveSlides: 1,
                slideWidth: 200,
                slideMargin: 20,
                nextSelector: '.slider-arrow--right',
                prevSelector: '.slider-arrow--left',
                auto: true,
                responsive: true

            };

        }
        if (window.innerWidth > 2560) {
            sliderViewedParams = {
                controls: true,
                pager: false,
                minSlides: 9,
                maxSlides: 9,
                moveSlides: 1,
                slideWidth: 220,
                slideMargin: 20,
                nextSelector: '.slider-arrow--right',
                prevSelector: '.slider-arrow--left',
                auto: true,
                responsive: true

            };
        }
        sliderViewed.reloadSlider(sliderViewedParams);
    });
})();