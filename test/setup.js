(function() {

    var  onSuccess = function(data) {
        console.log(data);
    };
    var onError = function(message) {
        console.log(message);
    };

    var form = document.querySelector('#test-form'); //находим форму в DOM
    form.addEventListener('submit', function(e) { //добавляем обработчик по отправке формы
        e.preventDefault(); //запрещаем действие по умолчанию
        var data = new FormData(form); //получаем данные с формы
        // var dataBlock = document.querySelector('.dannye');
        // var dataId = dataBlock.getAttribute('data-id');
        // var data = {$id: dataId , $request: 'motherofgod'}; //пример данных не с формы
        var url = 'ajax.php'; //указываем путь к скрипту на сервере
        var xhr = new XMLHttpRequest(); //создаем объект XMLHttpRequest

        xhr.addEventListener('load', function() { //обработчик ответа сервера
            if (xhr.status === 200) { //если статус ответа успешно
                onSuccess(xhr.responseText); //выполняем функцию onSuccess, в которую передаем данные от сервера
            } else { //иначе
                onError('Статус ответа: '+ xhr.status + ' ' + xhr.statusText); //выполняем функцию onError
            }
        });
        xhr.addEventListener('error', function(){ //обработчик, для ошибки 5хх
            onError('Произошла ошибка соединения');
        });
        xhr.addEventListener('timeout', function() { //обработчик, если время ожидания запроса вышло
            onError('Запрос не успел выполниться за ' + xhr.timeout + 'мс')
        });

        xhr.timeout = 30 * 1000; //устанавливаем время ожидания
        xhr.open('POST', url); //открываем соединение
        //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); //обязательно указываем, если отправляем данные не с формы!!!
        //xhr.send('param=' + JSON.stringify(data)); //отправляем данные не из формы
         xhr.send(data);//отправляем данные с формы
    });


})();
